clear;clc;
imfolder = 'png_crop\g';
outfolder = 'out\g';
reffolder = 'png_crop\r';
maskfolder = 'mask_ex_5';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end

start_id = 1;
end_id = 644;
scale = 256;

height = scale;
width = scale; 

flg = 0; %no zero
for id = start_id:end_id
    disp(id);
    tic
    im = im2double(imread([imfolder '\' num2str(id, '%05d') '.png']));
    mask = im2double(imread([maskfolder '\mask_' num2str(id, '%05d') '.png']));
    msk = padarray(mask, [5, 5]);
    ref = im2double(imread([reffolder '\' num2str(id, '%05d') '.png']));
    ref = padarray(ref, [5, 5], 'replicate');
    im = padarray(im, [5, 5], 'replicate');
    res_img = possion_editting(im, ref, msk, flg);
    targetSize = [height, width];
    r = centerCropWindow2d(size(res_img),targetSize);
    res_img = imcrop(res_img, r);
    im = imcrop(im, r);
    im_out = res_img.*mask + im.*(1 - mask);
    imwrite(im_out, [outfolder '\' num2str(id, '%05d') '.png']);
    toc
end