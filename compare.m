clear;clc;
imfolder_1 = 'png_crop_1';
imfolder_2 = 'out\2\com';
maskfolder = 'mask_ex_5';
outfolder = 'out\compare_2';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
start_id = 1;
end_id = 644;
scale = 256;
im_out = zeros(scale, 2*scale, 3);
im_mask = zeros(scale, 2*scale, 1);
for id = start_id:end_id
    disp(id);
    tic
    im_1 = im2double(imread([imfolder_1 '\' num2str(id, '%05d') '.png']));
    im_2 = im2double(imread([imfolder_2 '\' num2str(id, '%05d') '.png']));
    mask = im2double(imread([maskfolder '\mask_' num2str(id, '%05d') '.png']));
    im_out(1:scale, 1:scale, :) = im_1(1:scale, 1:scale, :);
    im_out(1:scale, scale+1:2*scale, :) = im_2(1:scale, scale+1:2*scale, :);
    im_mask(1:scale, 1:scale, :) = mask;
    im_mask(1:scale, scale+1:2*scale, :) = mask;
    im_mask(im_mask == 0) = 0.8;
    im_out = im_out.*repmat(im_mask, 1, 1, 3);
    imwrite(im_out, [outfolder '\' num2str(id, '%05d') '.png']);
    %imwrite(im_out, [outfolder '\' num2str(id, '%05d') '.png'], 'Alpha', im_mask);
    toc
end