clear;clc;
patch = 2;
imfolder_full = 'out\2\single';
imfolder_ori = ['png_crop_' num2str(patch)];
imfolder_full_r = 'png\r';
imfolder_full_b = 'png\b';
imfolder_r = 'out\3\r';
imfolder_g = ['png_crop_' num2str(patch) '\g'];
imfolder_b = 'out\3\b';
outfolder = 'out\3\single';
outfolder_r = 'out\3\single_r';
outfolder_b = 'out\3\single_b';
outfolder_com = 'out\3\com';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if ~exist(outfolder_com, 'dir')
    mkdir(outfolder_com);
end
if ~exist(outfolder_r, 'dir')
    mkdir(outfolder_r);
end
if ~exist(outfolder_b, 'dir')
    mkdir(outfolder_b);
end
if patch == 1
    x = 1230;
    y = 1197;
    scale = [256, 256];
elseif patch == 2
    x = 565;
    y = 1253;
    scale = [200, 300];
end
start_id = 1;
end_id = 644;
height = scale(1);
width = scale(2);
im_out = zeros(height, width, 3);
im_out_com = zeros(height, 2*width, 3);
for id = start_id:end_id
    disp(id);
    tic
    im_r = im2double(imread([imfolder_r '\' num2str(id, '%05d') '.png']));
    im_g = im2double(imread([imfolder_g '\' num2str(id, '%05d') '.png']));
    im_b = im2double(imread([imfolder_b '\' num2str(id, '%05d') '.png']));
    im_out(:, :, 1) = im_r;
    im_out(:, :, 2) = im_g;
    im_out(:, :, 3) = im_b;
    im_ori = im2double(imread([imfolder_ori '\' num2str(id, '%05d') '.png']));
    im_out_com(1:height, 1:width, :) = im_ori;
    im_out_com(1:height, width+1:2*width, :) = im_out;
    im_full = im2double(imread([imfolder_full '\' num2str(id, '%05d') '.bmp']));
    im_full(y+1:y+height, x+1:x+width, :) = im_out;
    im_full_r = im2double(imread([imfolder_full_r '\' num2str(id, '%05d') '.png']));
    im_full_r(y+1:y+height, x+1:x+width, :) = im_r;
    im_full_b = im2double(imread([imfolder_full_b '\' num2str(id, '%05d') '.png']));
    im_full_b(y+1:y+height, x+1:x+width, :) = im_b;
    imwrite(im_full_r, [outfolder_r '\' num2str(id, '%05d') '.png']);
    imwrite(im_full_b, [outfolder_b '\' num2str(id, '%05d') '.png']);
    imwrite(im_full, [outfolder '\' num2str(id, '%05d') '.bmp']);
    imwrite(im_out_com, [outfolder_com '\' num2str(id, '%05d') '.png']);
    toc
end