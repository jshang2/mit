function A = possion_editing_MatA(msk)

[m,n] = size(msk);


%% cut of boundary 
msk(1:end,1) = 0;
msk(1:end,end) = 0;
msk(1,1:end) = 0;
msk(end,1:end) = 0;

%% calculate A matrix size
msk_d = im2double(msk);
msk_sum = round(sum(msk_d(:)));

vec_count = m*n-msk_sum + msk_sum * 4 * 2;

i_vec = zeros(vec_count,1);
j_vec = zeros(vec_count,1);
s_vec = zeros(vec_count,1);

%% build A matrix

eq_id = 1;
count = 1;

for i = 1:m
    for j = 1:n
        if(msk(i,j) == 0) %copy original pixel
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j;
            s_vec(count,1) = 1;            
            count = count + 1;
            eq_id = eq_id + 1;
            
        else % add 4 gradient equaions
            % equation 1
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j;
            s_vec(count,1) = 1;
            count = count + 1;
            
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-2)*n + j;
            s_vec(count,1) = -1;
            count = count + 1;
            
            eq_id = eq_id + 1;
            
            % equation 2
            i_vec(count,1) = eq_id;
            j_vec(count,1) = i*n + j;
            s_vec(count,1) = 1;
            count = count + 1;
            
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j;
            s_vec(count,1) = -1;
            count = count + 1;
            
            eq_id = eq_id + 1;
            
            % equation 3
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j;
            s_vec(count,1) = 1;
            count = count + 1;
            
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j-1;
            s_vec(count,1) = -1;
            count = count + 1;
            
            eq_id = eq_id + 1;
            
            % equation 4
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j+1;
            s_vec(count,1) = 1;
            count = count + 1;
            
            i_vec(count,1) = eq_id;
            j_vec(count,1) = (i-1)*n + j;
            s_vec(count,1) = -1;
            count = count + 1;
            
            eq_id = eq_id + 1;
            
        end
    end
end

A = sparse(i_vec, j_vec, s_vec, eq_id-1, m*n);

return