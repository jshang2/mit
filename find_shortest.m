clear;clc;
win_size = 1;

mask_folder = 'mask';
mask = im2double(imread([mask_folder '\mask1.png']));
[M,N] = size(mask);
image_folder = 'png_crop\g';
image_folder_r1 = 'png_crop\r';
image_folder_r2 = 'png_crop\b';

use_opposite = 1; % use opposite
use_log = 1; % using laplacian of guassian
im_c_list = dir([image_folder '/*.png']);
% mask_folder = ['mask_' num2str(patch)];
% msk_num = size(im_c_list,1);

count = 0;
st_limit = 1;
end_limit = 644;
st_id = 1;
end_id = 644;
sigma = win_size;
im_rang = win_size;

if use_log == 1 && use_opposite == 1
    h_log = fspecial('log',5);
    line_folder = ['line_wingo_' num2str(win_size)];
    refined_mask_folder = ['refined_mask_wingo_' num2str(win_size)];
elseif use_log == 1
    h_log = fspecial('log',5);
    line_folder = ['line_wing_' num2str(win_size)];
    refined_mask_folder = ['refined_mask_wing_' num2str(win_size)];
elseif use_opposite == 1
    line_folder = ['line_wino_' num2str(win_size)];
    refined_mask_folder = ['refined_mask_wino_' num2str(win_size)];
else
    line_folder = ['line_win_' num2str(win_size)];
    refined_mask_folder = ['refined_mask_win_' num2str(win_size)];
end

if ~exist(line_folder, 'dir')
    mkdir(line_folder);
end
if ~exist(refined_mask_folder, 'dir')
    mkdir(refined_mask_folder);
end
h = fspecial('gaussian',[1,im_rang],sigma);
start_numrow = 1;
end_numrow = 1; % 5 if have enough time
path_mem = cell(end_id - st_id + 1, 1);
%for i = [59, 158, 159, 366, 498, 500, 501, 502, 504, 507, 509]
for i = st_id:end_id
    disp(i);
    tic

    im_tot = zeros(M, N, 3);
    %%%%%%%%%%%%%%%
    %-M-1 -1  M-1 %
    %-M    *  M   %
    %-M+1  1  M+1 %
    %%%%%%%%%%%%%%%
    %[-M-1, -M, -M+1, -1, 1, M-1, M, M+1]
    %StartNode = find(mask ~=0, start_num, 'first');
    [row, column, dataValues] = find(mask ~= 0);
    r_st = max(row);
    column_max = column(row == r_st);
    %c_st = round(min(column_max));
    c_st = column_max;
    StartNode_temp = c_st*M + r_st;%StartNode_temp = (c_st-1)*M + r_st;
    r_end = min(row);
    column_min = column(row == r_end);
    %c_end = round(mean(column_min));
    c_end = column_min;
    EndNode_temp = c_end*M + r_end;%EndNode_temp = (c_end-1)*M + r_end;
    var = [-M-1, -M, -1, M-1, M];
    
    st = max(i-(im_rang-1)/2, st_limit);
    et = min(i+(im_rang-1)/2, end_limit);
    for j = st:et
        if use_log == 1
            im_input = im2double(imread([image_folder '/' im_c_list(j).name]));
            if use_opposite == 1
                im_temp_r1 = im2double(imread([image_folder_r1 '/' im_c_list(j).name]));
                im_temp_r2 = im2double(imread([image_folder_r2 '/' im_c_list(j).name]));
                %im_temp = (im_temp + (1-im_temp_r1))/2;
                %im_temp = (im_temp + (1-im_temp_r1) + (1-im_temp_r2))/3;
                im_input = (2*im_input + (1-im_temp_r1) + (1-im_temp_r2))/4;
            end
            im_log = 1 - imfilter(im_input,h_log,'same');
            im_temp = im_log.*h(j-( i-(im_rang-1)/2)+1);
        else
            im_temp = im2double(imread([image_folder '/' im_c_list(j).name]));
            if use_opposite == 1
                im_temp_r1 = im2double(imread([image_folder_r1 '/' im_c_list(j).name]));
                im_temp_r2 = im2double(imread([image_folder_r2 '/' im_c_list(j).name]));
                %im_temp = (im_temp + (1-im_temp_r1))/2;
                %im_temp = (im_temp + (1-im_temp_r1) + (1-im_temp_r2))/3;
                im_temp = (2*im_temp + (1-im_temp_r1) + (1-im_temp_r2))/4;
            end
            im_temp = im_temp.*h(j-( i-(im_rang-1)/2)+1);
        end
        if(j == st)
            im_tot = im_temp;
        else
            im_tot = im_tot + im_temp;
        end

    end

    %figure;imshow(im_tot,[]);
    CostMat = 1 - mask + im_tot.*mask;

    %figure;imshow(CostMat,[]);
    D = im2graph_flex(CostMat, var);
    
    l_s = length(StartNode_temp);
    start_num = start_numrow*l_s;
    StartNode = zeros(1, start_num);
    for st_row = 1:start_numrow
        StartNode(1+(st_row-1)*l_s:l_s+(st_row-1)*l_s) = StartNode_temp-st_row+1;
    end    
    mean_array_st = zeros(start_num, 1);
    for idx = 1:start_num
        [dist, path, pred] = graphshortestpath(D, StartNode(idx), EndNode_temp(end));
        mean_array_st(idx) = mean(im_tot(path));
    end
    [~,min_id_st] = min(mean_array_st);

    e_s = length(EndNode_temp);
    end_num = end_numrow*e_s;
    EndNode = zeros(1, end_num);
    for end_row = end_numrow:-1:1
        EndNode(1+(end_row-1)*e_s:e_s+(end_row-1)*e_s) = EndNode_temp+end_row-1;
    end 
    mean_array_end = zeros(end_num, 1);
    for idx = 1:end_num
        [dist, path, pred] = graphshortestpath(D, StartNode(min_id_st), EndNode(idx));
        mean_array_end(idx) = mean(im_tot(path));
    end
    [~,min_id_end] = min(mean_array_end);
    
    [dist, path, pred] = graphshortestpath(D, StartNode(min_id_st), EndNode(min_id_end));    
    %[dist, path, pred] = graphshortestpath(D, StartNode(min_id_st), EndNode);
    %[dist, path, pred] = graphshortestpath(D, StartNode, EndNode);
    
    ShowMat = im_tot.*mask;
    %figure;imshow(ShowMat, []);
    [y, x] = ind2sub([M N], path);
    %hold on; plot(x, y, '-.');
    %set(gca,'YDir','reverse');
    im = im2double(imread([image_folder '/' im_c_list(i).name]));
    line_shape = reshape([x;y], 1, []);
    im_out_line = insertShape(im, 'Line', line_shape, 'Color', 'yello', 'LineWidth',1);
    %figure;imshow(im_out_line, []);
    im_combine = zeros(M,2*N,3);
    im_combine(:,1:N,:) = repmat(im, 1, 1, 3);
    im_combine(:,N+1:2*N,:) = im_out_line;
    imwrite(im_combine,[line_folder '/' 'line_' num2str(i,'%05d') '.png']);
    
    path2 = [x; y]';
    path_mem{i} = path2;
    msk_new = mask_from_line(path2, [M,N]);
    %figure;imshow(msk_new,[]);
    imwrite(msk_new,[refined_mask_folder '/' 'mask_' num2str(i,'%05d') '.png']);
    toc
end
save('Path.mat', 'path_mem');