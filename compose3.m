clear;clc;
patch = 3;
imfolder_full = 'out\3\single';
imfolder_ori = ['png_crop_' num2str(patch)];
imfolder_r = ['png_crop_' num2str(patch) '\r'];
imfolder_full_g = 'png\g';
imfolder_g = 'out\4\g';
imfolder_b = ['png_crop_' num2str(patch) '\b'];
outfolder = 'out\4\single';
outfolder_g = 'out\4\single_g';
outfolder_com = 'out\4\com';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if ~exist(outfolder_com, 'dir')
    mkdir(outfolder_com);
end
if ~exist(outfolder_g, 'dir')
    mkdir(outfolder_g);
end
if patch == 1
    x = 1230;
    y = 1197;
    scale = [256, 256];
elseif patch == 2
    x = 565;
    y = 1253;
    scale = [200, 300];
elseif patch == 3
    x= 300;
    y = 1375;
    scale = [78, 1475];
end
start_id = 1;
end_id = 644;
height = scale(1);
width = scale(2);
im_out = zeros(height, width, 3);
im_out_com = zeros(2*height, width, 3);
for id = start_id:end_id
    disp(id);
    tic
    im_r = im2double(imread([imfolder_r '\' num2str(id, '%05d') '.png']));
    im_g = im2double(imread([imfolder_g '\' num2str(id, '%05d') '.png']));
    im_b = im2double(imread([imfolder_b '\' num2str(id, '%05d') '.png']));
    im_out(:, :, 1) = im_r;
    im_out(:, :, 2) = im_g;
    im_out(:, :, 3) = im_b;
    im_ori = im2double(imread([imfolder_ori '\' num2str(id, '%05d') '.png']));
    im_out_com(1:height, 1:width, :) = im_ori;
    im_out_com(height+1:2*height, 1:width, :) = im_out;
    im_full = im2double(imread([imfolder_full '\' num2str(id, '%05d') '.bmp']));
    im_full(y+1:y+height, x+1:x+width, :) = im_out;
    im_full_g = im2double(imread([imfolder_full_g '\' num2str(id, '%05d') '.png']));
    im_full_g(y+1:y+height, x+1:x+width, :) = im_g;
    imwrite(im_full_g, [outfolder_g '\' num2str(id, '%05d') '.png']);
    imwrite(im_full, [outfolder '\' num2str(id, '%05d') '.bmp']);
    imwrite(im_out_com, [outfolder_com '\' num2str(id, '%05d') '.png']);
    toc
end