clear;clc;
imfolder = 'png_crop\g';
outfolder = 'out\g';
reffolder = 'png_crop\r';
maskfolder = 'mask';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if ~exist(maskfolder, 'dir')
    mkdir(maskfolder);
end

start_id = 1;
end_id = 644;
scale = 256;
x1 = 114;
y1 = 87;
x2 = 162;
y2 = 256;
mask = zeros(scale, scale);
mask(y1+1:y2, x1+1:x2) = 1;
imwrite(mask, [maskfolder '\mask1.png']);
im_size = size(mask);
height = im_size(1);
width = im_size(2); 
msk = padarray(mask, [5, 5]);

flg = 0; %no zero
for id = start_id:end_id
    disp(id);
    tic
    im = im2double(imread([imfolder '\' num2str(id, '%05d') '.png']));
    ref = im2double(imread([reffolder '\' num2str(id, '%05d') '.png']));
    ref = padarray(ref, [5, 5], 'replicate');
    im = padarray(im, [5, 5], 'replicate');
    res_img = possion_editting(im, ref, msk, flg);
    targetSize = [height, width];
    r = centerCropWindow2d(size(res_img),targetSize);
    res_img = imcrop(res_img, r);
    im = imcrop(im, r);
    im_out = res_img.*mask + im.*(1 - mask);
    imwrite(im_out, [outfolder '\' num2str(id, '%05d') '.png']);
    toc
end