clear;clc;
imfolder_full = 'png';
imfolder_ori = 'png_crop_1';
imfolder_r = 'png_crop_1\r';
imfolder_full_g = 'png\g';
imfolder_g = 'out\2\g';
imfolder_b = 'png_crop_1\b';
outfolder = 'out\2\single';
outfolder_g = 'out\2\single_g';
outfolder_com = 'out\2\com';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if ~exist(outfolder_com, 'dir')
    mkdir(outfolder_com);
end
if ~exist(outfolder_g, 'dir')
    mkdir(outfolder_g);
end
x = 1230;
y = 1197;
start_id = 1;
end_id = 644;
scale = 256;
im_out = zeros(scale, scale, 3);
im_out_com = zeros(scale, 2*scale, 3);
for id = start_id:end_id
    disp(id);
    im_r = im2double(imread([imfolder_r '\' num2str(id, '%05d') '.png']));
    im_g = im2double(imread([imfolder_g '\' num2str(id, '%05d') '.png']));
    im_b = im2double(imread([imfolder_b '\' num2str(id, '%05d') '.png']));
    im_out(:, :, 1) = im_r;
    im_out(:, :, 2) = im_g;
    im_out(:, :, 3) = im_b;
    im_ori = im2double(imread([imfolder_ori '\' num2str(id, '%05d') '.png']));
    im_out_com(1:scale, 1:scale, :) = im_ori;
    im_out_com(1:scale, scale+1:2*scale, :) = im_out;
    im_full = im2double(imread([imfolder_full '\' num2str(id, '%05d') '.png']));
    im_full(y+1:y+scale, x+1:x+scale, :) = im_out;
    im_full_g = im2double(imread([imfolder_full_g '\' num2str(id, '%05d') '.png']));
    im_full_g(y+1:y+scale, x+1:x+scale, :) = im_g;
    imwrite(im_full_g, [outfolder_g '\' num2str(id, '%05d') '.png']);
    imwrite(im_full, [outfolder '\' num2str(id, '%05d') '.bmp']);
    imwrite(im_out_com, [outfolder_com '\' num2str(id, '%05d') '.png']);
end