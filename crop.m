clear;clc;
patch = 3;
outfolder = ['png_crop_' num2str(patch)];
if patch == 1
    imfolder = 'png';   
elseif patch == 2
    imfolder = 'out\2\single';
elseif patch == 3
    imfolder = 'out\3\single';
end
imfolder_r = 'png\r';
imfolder_g = 'png\g';
imfolder_b = 'png\b';
maskfolder = 'mask';
outfolder_r = ['png_crop_' num2str(patch) '\r'];
outfolder_g = ['png_crop_' num2str(patch) '\g'];
outfolder_b = ['png_crop_' num2str(patch) '\b'];
if ~exist(imfolder_r, 'dir')
    mkdir(imfolder_r);
end
if ~exist(imfolder_g, 'dir')
    mkdir(imfolder_g);
end
if ~exist(imfolder_b, 'dir')
    mkdir(imfolder_b);
end
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if ~exist(outfolder_r, 'dir')
    mkdir(outfolder_r);
end
if ~exist(outfolder_g, 'dir')
    mkdir(outfolder_g);
end
if ~exist(outfolder_b, 'dir')
    mkdir(outfolder_b);
end
start_id = 1;
end_id = 644;
if patch == 1
    x = 1230;
    y = 1197;
    size = [256, 256];
elseif patch == 2
    x = 565;
    y = 1253;
    size = [200, 300];
elseif patch == 3
    x= 300;
    y = 1375;
    size = [78, 1475];
end
height = size(1);
width = size(2);
mask = im2double(imread([maskfolder '\mask' num2str(patch) '_full.png']));
im_crop_mask = mask(y+1:y+height, x+1:x+width, :);
imwrite(im_crop_mask, [maskfolder '\mask' num2str(patch) '.png']);
for id = start_id:end_id
    disp(id);
    tic
    if patch == 1
        im = im2double(imread([imfolder '\' num2str(id, '%05d') '.png']));
    else
        im = im2double(imread([imfolder '\' num2str(id, '%05d') '.bmp']));
    end
    im_r = im(:, :, 1);
    im_g = im(:, :, 2);
    im_b = im(:, :, 3); 
    imwrite(im_r, [imfolder_r '\' num2str(id, '%05d') '.png']);
    imwrite(im_g, [imfolder_g '\' num2str(id, '%05d') '.png']);
    imwrite(im_b, [imfolder_b '\' num2str(id, '%05d') '.png']);
    im_crop = im(y+1:y+height, x+1:x+width, :);
    im_crop_r = im_crop(:, :, 1);
    im_crop_g = im_crop(:, :, 2);
    im_crop_b = im_crop(:, :, 3);
    imwrite(im_crop_r, [outfolder_r '\' num2str(id, '%05d') '.png']);
    imwrite(im_crop_g, [outfolder_g '\' num2str(id, '%05d') '.png']);
    imwrite(im_crop_b, [outfolder_b '\' num2str(id, '%05d') '.png']);
    imwrite(im_crop, [outfolder '\' num2str(id, '%05d') '.png']);
    toc
end