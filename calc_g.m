function [grow, gcol] = calc_g(img)

img_row_shift = circshift(img,1,1);
img_col_shift = circshift(img,1,2);
        
grow = img - img_row_shift;
gcol = img - img_col_shift;

return;