clear;clc;
patch = 3; %patch ~= 1
if patch == 2
    imfolder_r = 'png\r';
    imfolder_b = 'png\b';
elseif patch == 3
    imfolder_r = 'out\3\single_r';
    imfolder_b = 'out\3\single_b';
end
imfolder_g = 'out\2\single_g';

outfolder = 'com';
if ~exist(outfolder, 'dir')
    mkdir(outfolder)
end
start_id = 1;
end_id = 644;
im_tot_r = zeros(1556, 2048);
im_tot_g = zeros(1556, 2048);
im_tot_b = zeros(1556, 2048);
for id = start_id:end_id
    disp(id)
    im_r = im2double(imread([imfolder_r '\' num2str(id, '%05d') '.png']));
    im_g = im2double(imread([imfolder_g '\' num2str(id, '%05d') '.png']));
    im_b = im2double(imread([imfolder_b '\' num2str(id, '%05d') '.png']));
    im_tot_r = im_tot_r + im_r;
    im_tot_g = im_tot_g + im_g;
    im_tot_b = im_tot_b + im_b;
end
im_tot_r = im_tot_r/(end_id-start_id+1);
im_tot_g = im_tot_g/(end_id-start_id+1);
im_tot_b = im_tot_b/(end_id-start_id+1);
imwrite(im_tot_r, [outfolder '\com_r.png']);
imwrite(im_tot_g, [outfolder '\com_g.png']);
imwrite(im_tot_b, [outfolder '\com_b.png']);
save('im.mat', 'im_tot_r', 'im_tot_g', 'im_tot_b');

    