clear;clc;
patch = 2;
imfolder_1 = ['png_crop_' num2str(patch) '\r'];
imfolder_2 = ['png_crop_' num2str(patch) '\b'];
outfolder_1 = 'out\3\r';
outfolder_2 = 'out\3\b';
reffolder = ['png_crop_' num2str(patch) '\g'];
maskfolder = 'mask';
if ~exist(outfolder_1, 'dir')
    mkdir(outfolder_1);
end
if ~exist(outfolder_2, 'dir')
    mkdir(outfolder_2);
end

start_id = 1;
end_id = 644;
% scale = 256;
% x1 = 114;
% y1 = 87;
% x2 = 162;
% y2 = 256;
% mask = zeros(scale, scale);
% mask(y1+1:y2, x1+1:x2) = 1;
% imwrite(mask, [maskfolder '\mask1.png']);
mask = im2double(imread([maskfolder '\mask' num2str(patch) '_ex.png']));
im_size = size(mask);
height = im_size(1);
width = im_size(2); 
msk = padarray(mask, [5, 5]);

flg = 0; %no zero
for id = start_id:end_id
    disp(id);
    tic
    im_1 = im2double(imread([imfolder_1 '\' num2str(id, '%05d') '.png']));
    im_2 = im2double(imread([imfolder_2 '\' num2str(id, '%05d') '.png']));
    ref = im2double(imread([reffolder '\' num2str(id, '%05d') '.png']));
    ref = padarray(ref, [5, 5], 'replicate');
    im_1 = padarray(im_1, [5, 5], 'replicate');
    im_2 = padarray(im_2, [5, 5], 'replicate');
    res_img_1 = possion_editting(im_1, ref, msk, flg);
    res_img_2 = possion_editting(im_2, ref, msk, flg);
    targetSize = [height, width];
    r = centerCropWindow2d(size(res_img_1),targetSize);
    res_img_1 = imcrop(res_img_1, r);
    im_1 = imcrop(im_1, r);
    im_out_1 = res_img_1.*mask + im_1.*(1 - mask);
    imwrite(im_out_1, [outfolder_1 '\' num2str(id, '%05d') '.png']);
    r = centerCropWindow2d(size(res_img_2),targetSize);
    res_img_2 = imcrop(res_img_2, r);
    im_2 = imcrop(im_2, r);
    im_out_2 = res_img_2.*mask + im_2.*(1 - mask);
    imwrite(im_out_2, [outfolder_2 '\' num2str(id, '%05d') '.png']);
    toc
end