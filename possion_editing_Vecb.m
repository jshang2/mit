function b = possion_editing_Vecb(src_img,ref_grow,ref_gcol,msk)

[m,n] = size(msk);


%% cut of boundary 
msk(1:end,1) = 0;
msk(1:end,end) = 0;
msk(1,1:end) = 0;
msk(end,1:end) = 0;

%% calculate A matrix size
msk_d = im2double(msk);
msk_sum = round(sum(msk_d(:)));

eq_count = m*n-msk_sum + msk_sum * 4;

b = zeros(eq_count,1);

%% build b vector

count = 1;

for i = 1:m
    for j = 1:n
        if(msk(i,j) == 0) %copy original pixel
            b(count,1) = src_img(i,j);
            count = count + 1;
        else % add 4 gradient equaions
            b(count,1) = ref_grow(i,j);
            count = count + 1;
            
            b(count,1) = ref_grow(i+1,j);
            count = count + 1;
            
            b(count,1) = ref_gcol(i,j);
            count = count + 1;
            
            b(count,1) = ref_gcol(i,j+1);
            count = count + 1;
        end
    end
end


return