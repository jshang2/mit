clear;clc;
patch = 3; %patch ~=1
if patch == 2
    thres = 0.999;
elseif patch == 3
    thres = 0.99;
end
load('im.mat');
h_log = fspecial('log',5);
n_f = ones(3, 3);
n_f(2, 2) = -8;
maskfolder = 'mask';
if patch == 3
    im_diff = im_tot_g;
    %figure;imshow(im_diff, []);
    im_log = 1 - imfilter(im_diff,h_log,'same');
    %figure;imshow(im_log, []);
    im_out = im_log.*(im_log<thres);
    figure;imshow(im_out, []);
    im_out_f = medfilt2(im_out);
    figure;imshow(im_out_f, []);
elseif patch == 2
    im_diff = (im_tot_r + im_tot_b)/2;
    %figure;imshow(im_diff, []);
    im_log = 1 - imfilter(im_diff,h_log,'same');
    %figure;imshow(im_log, []);
    im_out = im_log.*(im_log<thres);
    %figure;imshow(im_out, []);
    im_out_f = medfilt2(im_out);
    %figure;imshow(im_out_f, []);
    im_out_f = medfilt2(im_out_f);
    im_out_f = medfilt2(im_out_f);
    im_out_f = medfilt2(im_out_f);
    im_out_f = medfilt2(im_out_f);
    im_out_f = medfilt2(im_out_f);
    im_out_f = medfilt2(im_out_f);
    %im_out_f = imfilter(im_out,n_f,'same');
    %im_out = im_out.*(1-(im_out_f<-6.9));
    figure;imshow(im_out_f, []);
end
imwrite(im_out_f, [maskfolder, '\mask' num2str(patch) '_full.png']);