clear;clc;
input_folder = 'refined_mask';
start_id = 1;
end_id = 644;
kernel_size = 4;
outfolder = ['mask_ex_' num2str(kernel_size)];
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
for id = start_id:end_id
    disp(id);
	msk = im2double(imread([input_folder '\mask_' num2str(id, '%05d') '.png']));
	ker = ones(kernel_size);
	msk_c = conv2(msk, ker, 'same');
	imwrite(msk_c, [outfolder '\mask_' num2str(id, '%05d') '.png']);
end

