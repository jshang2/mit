function msk = mask_from_line(line_points, msk_siz)

    msk = zeros(msk_siz);
    
    pt_num = size(line_points,1);
    
    for i = 2:pt_num
        st_pt = round(line_points(i-1,:));
        end_pt = round(line_points(i,:));
        
        r1 = st_pt(2);
        c1 = st_pt(1);
        
        r2 = end_pt(2);
        c2 = end_pt(1);
        
        col_v = abs(c2 - c1);
        row_v = abs(r2 - r1);
        
        if(col_v > row_v)
            if(c2 > c1)
                step = 1;
            else
                step = -1;
            end
            
            for c = c1:step:c2
                r = round((c-c1)*(r2-r1)/(c2-c1) + r1);
                msk(r,c) = 1;
            end
        elseif(col_v < row_v)
            if(r2 > r1)
                step = 1;
            else
                step = -1;
            end
            
            for r = r1:step:r2
                
                c = round((r-r1)*(c2-c1)/(r2-r1) + c1);
                msk(r,c) = 1;
            end
        elseif(col_v ~= 0)
            if(r2 > r1)
                step = 1;
            else
                step = -1;
            end
            
            for r = r1:step:r2
                
                c = round((r-r1)*(c2-c1)/(r2-r1) + c1);
                msk(r,c) = 1;
            end
        end
        
        
        
    end

return;

