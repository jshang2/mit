clear;clc;
patch = 3;
input_folder = 'mask';
outfolder = 'mask';
if ~exist(outfolder, 'dir')
    mkdir(outfolder);
end
if patch == 2
    kernel_size = 4;
elseif patch == 3
    kernel_size = 3;
end
ker = ones(kernel_size);
msk = im2double(imread([input_folder '\mask' num2str(patch) '.png']));
im_size = size(msk);
height = im_size(1);
width = im_size(2);
msk_c = conv2(msk, ker, 'same');
if patch == 2
    x = 235;
    y = 140;
    msk_c(1:y, x:end, :) = 0;
elseif patch == 3
    x = 110;
    y = 40;
    msk_c(y:end, x:x+35, :) = 1;
    x2 = 165;
    y2 = 1;
    msk_c(y2:end, x2:x2+25, :) = 1;
end
imwrite(msk_c, [outfolder '\mask' num2str(patch) '_ex.png']);
% for id = start_id:end_id
%     disp(id);
%  	msk = im2double(imread([input_folder '\mask_' num2str(id, '%05d') '.png']));
% 	ker = ones(kernel_size);
% 	msk_c = conv2(msk, ker, 'same');
% 	imwrite(msk_c, [outfolder '\mask_' num2str(id, '%05d') '.png']);
% end

