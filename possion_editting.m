function res_img = possion_editting(src_img, ref_img, msk, flg)

    [m,n,chs] = size(src_img);
    
    res_img = zeros(m,n,chs);
    
    A = possion_editing_MatA(msk);
    
    ref_img_sh = zeros(m,n);
    src_img_sh = zeros(m,n);
    
    for channel = 1:chs % for color or mono image
        src_img_sh(:,:) = src_img(:,:,channel);
        ref_img_sh(:,:) =  ref_img(:,:,channel);
        
        if flg == 1
            [ref_grow, ref_gcol] = calc_g_zerox(ref_img_sh);
        elseif flg == 2
            [ref_grow, ref_gcol] = calc_g_zeroy(ref_img_sh);
        elseif flg == 3
            [ref_grow, ref_gcol] = calc_g_zeroxy(ref_img_sh);
        else %flg == 0
            [ref_grow, ref_gcol] = calc_g(ref_img_sh);
        end
        
        b = possion_editing_Vecb(src_img_sh,ref_grow,ref_gcol,msk);

        x = A\b;
        
        im_temp = reshape(x,n,m);
        res_img(:,:,channel) = im_temp';
    end

return